package com.cubic.vehicle.service;

import java.util.List;

import com.cubic.vehicle.data.VehicleData;
import com.cubic.vehicle.data.VehicleSearchResult;

public interface VehicleService {

	VehicleData find(final String vehicleVin);

	VehicleData save(final VehicleData vehicle);

	VehicleData update(final VehicleData vehicle);
	
	void remove(final String vehicleVin);

	VehicleData searchVehicleByVin(String vehicleVin);

	VehicleSearchResult searchVehicleByMake(String VehicleMake);

	VehicleSearchResult searchVehicleByModel(final String vehicleModel);
	
	//VehicleSearchResult searchByMake&Model(final String vehicleMake, final String vehicleModel);
	
	VehicleData searchVehicleByStatus(final boolean vehicleStatus);

	//List<VehicleData> getAllVehicles();
}
