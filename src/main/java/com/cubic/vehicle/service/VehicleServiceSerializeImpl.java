package com.cubic.vehicle.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.cubic.vehicle.data.RandomNumbers;
import com.cubic.vehicle.data.VehicleData;
import com.cubic.vehicle.data.VehicleSearchResult;
import com.cubic.vehicle.data.Vehicles;
import com.cubic.vehicle.db.VehicleDB;
import com.cubic.vehicle.exceptions.InvalidDataException;
import com.cubic.vehicle.exceptions.VehicleNotFoundException;

@Service
public class VehicleServiceSerializeImpl implements VehicleService {

	private VehicleDB dbService = new VehicleDB();

	@Override
	public VehicleData find(String vehicleVin) {
		VehicleData result = null;
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			result = vehicles.get(vehicleVin);
		}

		if (result == null) {
			try {
				throw new VehicleNotFoundException("Vehicle not in the system");
			} catch (VehicleNotFoundException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	
	/*@Override
	public List<VehicleData> getAllVehicles() {
		List<VehicleData> result = null;
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			result = vehicles.getAll();
		}

		if (result == null) {
			try {
				throw new VehicleNotFoundException("No vehicles not in the system");
			} catch (VehicleNotFoundException e) {
				e.printStackTrace();
			}
		}

		return result;
	}*/

	@Override
	public VehicleData save(final VehicleData vehicle) {
		// Validate the data
		if (vehicle == null || StringUtils.isEmpty(vehicle.getVehicleMake())
				|| StringUtils.isEmpty(vehicle.getVehicleModel())) {
			throw new InvalidDataException("Vehicle data is invalid");
		}

		if (vehicle.getVehicleVin() == null) {
			this.create(vehicle);
		} else {
			update(vehicle);
		}

		return vehicle;
	}

	public VehicleData update(final VehicleData vehicle) {
		final Vehicles vehicles = getVehicles();

		if (!vehicles.contains(vehicle.getVehicleVin())) {
			throw new VehicleNotFoundException("Vehicle not in the system");
		}

		vehicles.save(vehicle);
		dbService.persist(vehicles);
		return vehicle;
	}

	private VehicleData create(final VehicleData vehicle) {
		vehicle.setVehicleVin(RandomNumbers.generateString());
		Vehicles vehicles = getVehicles();
		vehicles.save(vehicle);
		dbService.persist(vehicles);
		return vehicle;
	}

	private Vehicles getVehicles() {
		Vehicles vehicles = dbService.get();
		if (vehicles == null) {
			vehicles = new Vehicles();
		}
		return vehicles;
	}

	@Override
	public void remove(final String vehicleVin) {
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			vehicles.delete(vehicleVin);
			dbService.persist(vehicles);
		}

	}

	public VehicleData searchVehicleByVin(String vehicleVin) {
		VehicleData result = null;
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			result = vehicles.get(vehicleVin);
		}

		if (result == null) {
			throw new VehicleNotFoundException("Vehicle not in the system");
		}

		return result;
	}

	public VehicleSearchResult searchVehicleByMake(String vehicleMake) {
		final VehicleSearchResult result = VehicleSearchResult.builder().build();
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			vehicles.getAll().forEach(e -> {
				if (matches(vehicleMake, e.getVehicleMake())) {
					result.getVehicles().add(e);
				}
			});
			;
		}
		return result;
	}

	public VehicleSearchResult searchVehicleByModel(String vehicleModel) {
		final VehicleSearchResult result = VehicleSearchResult.builder().build();
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			vehicles.getAll().forEach(e -> {
				if (matches(vehicleModel, e.getVehicleModel())) {
					result.getVehicles().add(e);
				}
			});
			;
		}
		return result;
	}

	@Override
	public VehicleData searchVehicleByStatus(boolean vehicleStatus) {
		VehicleData result = null;
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			result = vehicles.getStatus(vehicleStatus);
		}

		if (result == null) {
			throw new VehicleNotFoundException("Vehicle not in the system");
		}

		return result;
	}
	
	/*@Override
	public VehicleSearchResult searchByMake&Model(String vehicleMake, String vehicleModel) {
		final VehicleSearchResult result = VehicleSearchResult.builder().build();
		final Vehicles vehicles = dbService.get();
		if (vehicles != null) {
			vehicles.getAll().forEach(e -> {
				if (matches(vehicleMake, e.getVehicleMake()) || matches(vehicleModel, e.getVehicleModel())) {
					result.getVehicles().add(e);
				}
			});
			;
		}
		return result;
	}*/

	private boolean matches(String searchStr, String value) {
		return searchStr != null && value != null && value.toLowerCase().startsWith(searchStr.toLowerCase());
	}

}
