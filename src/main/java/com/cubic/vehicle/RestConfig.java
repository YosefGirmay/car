package com.cubic.vehicle;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.cubic.vehicle.resource.VehicleResource;

@Configuration
public class RestConfig extends ResourceConfig {

	public RestConfig() {
		//this.register(HelloWorld.class);
		this.register(VehicleResource.class);
		
	}
}
