package com.cubic.vehicle.test;

import com.cubic.vehicle.data.VehicleData;
import com.cubic.vehicle.data.Vehicles;
import com.cubic.vehicle.db.VehicleDB;

public class DBTest {

	public static void main(String[] args) {
		final VehicleDB db = new VehicleDB();
		Vehicles vehicles = db.get();
		if (vehicles == null)
			vehicles = new Vehicles();
		VehicleData vehicle = new VehicleData ("200","SSSSS","F1",2015,true);
		vehicles.save(vehicle);
		db.persist(vehicles);
		vehicles = db.get();
		System.out.println(vehicles.getAll());

	}
	

}
