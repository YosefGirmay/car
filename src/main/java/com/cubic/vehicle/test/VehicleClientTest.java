package com.cubic.vehicle.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cubic.vehicle.data.ErrorInfo;
import com.cubic.vehicle.data.VehicleData;

public class VehicleClientTest {

	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		Response response = client.target("http://localhost:8000/vehicle/100")
				.request(MediaType.APPLICATION_JSON).get();

		if (response.getStatus() >= 200 && response.getStatus() <= 299) {
			VehicleData vehicle = response.readEntity(VehicleData.class);
			System.out.println(vehicle);
		} else {
			System.out.println("Status Code =" + response.getStatus());
			ErrorInfo errorInfo = response.readEntity(ErrorInfo.class);
			System.out.println(errorInfo);
		}
	}
}