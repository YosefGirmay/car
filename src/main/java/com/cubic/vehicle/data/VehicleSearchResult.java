package com.cubic.vehicle.data;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VehicleSearchResult {

	@Builder.Default
	private List<VehicleData> vehicles = new ArrayList<VehicleData>();
}


