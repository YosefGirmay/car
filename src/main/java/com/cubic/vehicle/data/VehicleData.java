package com.cubic.vehicle.data;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VehicleData implements Serializable {

	private static final long serialVersionUID = 1L;
	private String vehicleVin;
	private String vehicleMake;
	private String vehicleModel;
	private int vehicleMakeYear;
	private boolean vehicleStatus;
	

	}
	


