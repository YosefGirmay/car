package com.cubic.vehicle.data;
import java.util.UUID;

public class RandomNumbers {

	   public static void main(String[] args) {
	        System.out.println(generateString());
	    }

	    public static String generateString() {
	        String uuid = UUID.randomUUID().toString();
	        uuid=uuid.substring(0, 20).toUpperCase();
	       return uuid.replace("-", "");
	    }
	}