package com.cubic.vehicle.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Vehicles implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final private Map<String, VehicleData> vehicles = new HashMap<String, VehicleData>();

	public void save(final VehicleData vehicle) {
		vehicles.put(vehicle.getVehicleVin(), vehicle);
	}

	public Boolean contains(String vehicleVin) {
		return vehicles.containsKey(vehicleVin);
	}

	public VehicleData get(String vehicleVin) {
		return vehicles.get(vehicleVin);
	}
	
	public VehicleData getStatus(boolean vehicleStatus) {
		return vehicles.get(vehicleStatus);
	}

	public void delete(String vehicleVin) {
		vehicles.remove(vehicleVin);
	}

	public List<VehicleData> getAll() {
		return new ArrayList<VehicleData>(vehicles.values());
	}

}
