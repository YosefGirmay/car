package com.cubic.vehicle.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cubic.vehicle.data.VehicleData;
import com.cubic.vehicle.data.VehicleSearchResult;
import com.cubic.vehicle.data.Vehicles;
import com.cubic.vehicle.exceptions.VehicleNotFoundException;
import com.cubic.vehicle.service.VehicleService;

@Service
@Path("/vehicle")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VehicleResource {

	@Autowired
	private VehicleService vs;

	@GET
	public Response healthCheck() {
		return Response.ok().build();
	}

	@GET
	@Path("/{vehicleVin}")
	public Response find(@PathParam("vehicleVin") final String vehicleVin) {
		final VehicleData entity = vs.find(vehicleVin);
		return Response.ok().entity(entity).build();
	}
	
	@GET
	@Path("/searchByMake")
	public Response searchVehicleByMake(@QueryParam("vehicleMake") final String vehicleMake) {
		final VehicleSearchResult entity = vs.searchVehicleByMake(vehicleMake);
		return Response.ok().entity(entity).build();
	}
	
	@GET
	@Path("/searchByModel")
	public Response searchVehicleByModel(@QueryParam("vehicleModel") final String vehicleModel) {
		final VehicleSearchResult entity = vs.searchVehicleByModel(vehicleModel);
		return Response.ok().entity(entity).build();
	}

	@GET
	@Path("/searchByStatus")
	public Response VehicleData(@QueryParam("vehicleStatus") final boolean vehicleStatus) {
		final VehicleData entity = vs.searchVehicleByStatus(vehicleStatus);
		return Response.ok().entity(entity).build();
	}
	
	@POST
	public Response createVehicle(final VehicleData vehicle) {
		final VehicleData entity = vs.save(vehicle);
		return Response.ok().entity(entity).build();
	}

	@PUT
	public Response modifyVehicle(final VehicleData vehicle) {
		vs.save(vehicle);
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{vehicleVin}")
	public Response removeVehicle(@PathParam("vehicleVin") final String vehicleVin) {
		vs.remove(vehicleVin);
		return Response.noContent().build();

	}

}
