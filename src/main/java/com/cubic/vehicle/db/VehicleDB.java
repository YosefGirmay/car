package com.cubic.vehicle.db;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.cubic.vehicle.data.Vehicles;

public class VehicleDB {

	private static final String FILE_PATH = "C:\\Develop\\vehicles.ser";

	public void persist(final Vehicles data) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(FILE_PATH));
			out.writeObject(data);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(out);
		}
	}

	private void close(Closeable stream) {
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Vehicles get() {
		Vehicles result = null;
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(FILE_PATH));
			result = (Vehicles) in.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(in);
		}

		return result;
	}

}
