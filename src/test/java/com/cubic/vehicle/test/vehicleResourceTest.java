package com.cubic.vehicle.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import com.cubic.vehicle.data.VehicleData;
import com.cubic.vehicle.data.VehicleSearchResult;
import com.cubic.vehicle.resource.VehicleResource;
import com.cubic.vehicle.service.VehicleService;
import com.cubic.vehicle.service.VehicleServiceSerializeImpl;

@RunWith(MockitoJUnitRunner.class)
public class vehicleResourceTest {

	private VehicleSearchResult response;
	private VehicleData entity;
	
	@Mock
	private VehicleService vs;
	
	@Spy
	private VehicleServiceSerializeImpl vssi;
	
	@InjectMocks
	private VehicleResource resource;
	
	/*@Before
	public void setUp() {
		response = resource.searchVehicleByMake("Toyota");	
		when(vssi.searchVehicleByMake("Toyota")).thenReturn(null);
	}*/
	
	@Test
	public void testFindVehicleByVin() {
		
	when(vs.find(any(String.class))).thenReturn(null);
	Response response = resource.find("5D70CCCF2B144923A");
	verify(vs).find(any(String.class));
	}

	@Test
	public void searchVehicleByMakeTest() {
		when(vs.searchVehicleByMake(any(String.class))).thenReturn(response);
		Response response = resource.searchVehicleByMake("Toyota");
		verify(vs).searchVehicleByMake("Toyota");
	}
	
	@Test
	public void searchVehicleByModelTest() {
		when(vs.searchVehicleByModel(any(String.class))).thenReturn(response);
		Response response = resource.searchVehicleByModel("Altima");
		verify(vs).searchVehicleByModel("Altima");
	}
	
	@Test
	public void searchVehicleByStatusTest() {
		when(vs.searchVehicleByStatus(any(Boolean.class))).thenReturn(entity);
		Response response = resource.VehicleData(true);
		verify(vs).searchVehicleByStatus(true);
	}
	
	@Test
	public void createVehicleTest() {
		when(vs.save(any(VehicleData.class))).thenReturn(entity);
		Response response = resource.createVehicle(entity);
		verify(vs).save(entity);
	}
	
	@Test
	public void updateVehicleTest() {
		when(vs.update(any(VehicleData.class))).thenReturn(entity);
		Response response = resource.modifyVehicle(entity);
		//verify(vs).update(entity);
	}
	
	/*@Test
	public void removeVehicleTest() {
		when(vs.remove(any(String.class))).thenReturn(null);
		Response response = resource.removeVehicle("5D70CCCF2B144923A");
		assertEquals("Honda", resource.removeVehicle("5D70CCCF2B144923A"));
	}*/
}
